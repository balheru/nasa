import datetime

import requests
# Some starter variables
key = "vXbhBYr4GRbrrBUQKRUtG0Jd51b298eh5tLcfyeH"
START_DATE = "2019-10-31"
END_DATE = "2019-11-02"
api_date = f"https://api.nasa.gov/neo/rest/v1/feed?start_date={START_DATE}&end_date={END_DATE}&api_key={key}"

START_DATE_1 = "2020-09-10"
END_DATE_2 = "2020-09-17"
api_date_2 = f"https://api.nasa.gov/neo/rest/v1/feed?start_date={START_DATE_1}&end_date={END_DATE_2}&api_key={key}"


def scrape(list):
    fast_list = []
    for k, v in list["near_earth_objects"].items():
        print("=====")
        print(k)
        print("=====")
        for z in v:
            fast_list.append(float(z["close_approach_data"][0]["relative_velocity"]["kilometers_per_second"]))
            print(z["id"], z["close_approach_data"][0]["relative_velocity"]["kilometers_per_second"], sep="|")
    print("======the list ========")
    for i in fast_list:
        print(i)
    print("=====")
    print("max vel")
    # It was so easy to forget that i was supposed to implement a manual max and min i accidently skipped it
    max_vel = 0
    for i in fast_list:
        if i > max_vel:
            max_vel = i
    print(max_vel)
    # print(max(fast_list))
    print("min vel")
    min_vel = fast_list[0]
    for i in fast_list:
        if i  < max_vel:
            min_vel = i
    print(min_vel)
    # print(min(fast_list))
    print("=====")
    print("mean vel")
    sum = 0.0
    for i in fast_list:
        sum += i
    print(sum / len(fast_list))
    print("=====")
    print("median vel")
    sorted(fast_list)
    lstLen = len(fast_list)
    index = (lstLen - 1) // 2
    if (lstLen % 2):
        print(fast_list[index])
    else:
        print(fast_list[index] + fast_list[index + 1]) / 2.0

# Q1 dates with all the stats
print(api_date)
r = requests.get(api_date)
j = r.json()

scrape(j)

# Q2 dates with all the stats
print(api_date_2)
r2 = requests.get(api_date_2)
j2 = r2.json()
scrape(j2)






# As in the bash script we will generate a list of days, from this point forward it, really is a re-implementation detail
date_list = []
Previous_Date= datetime.datetime.today()
date_list.append(Previous_Date)
hazard_list = []
for i in range(0,10000):
    # print(Previous_Date.strftime("%Y-%m-%d"))
    Previous_Date = Previous_Date - datetime.timedelta(days=1)
    # Q3 begins here
    # As in the bash script we do this day by day so we eagerly get the best 3 most recent hazardous asteroids
    api_date_3 = f"https://api.nasa.gov/neo/rest/v1/feed?start_date={Previous_Date}&end_date={Previous_Date}&api_key={key}"
    r3 = requests.get(api_date_3)
    j3 = r3.json()
    for k, v in j3["near_earth_objects"].items():
        # Debug to have a nice breakdown
        # print("=====")
        # print(k)
        # print("=====")
        for z in v:
            if z["is_potentially_hazardous_asteroid"]:
                hazard_list.append(
                    (z["id"],z["close_approach_data"][0]["epoch_date_close_approach"]))
            # Debug to see what is happening
            # print(
            #     z["id"],
            #     z["close_approach_data"][0]["epoch_date_close_approach"],
            #     z["is_potentially_hazardous_asteroid"],
            #     sep="|")
    if len(hazard_list) >= 3:
        break
# At this point it's probably good to sort our 3 extracted values
# And only display the top 3 most recent as we can potentiall have more than 3 in 1 loop
sorted(hazard_list, key = lambda x: x[1])
print("======the list of id's of the most recent asteroids, with their epoch date========")
for i in hazard_list:
    print(i[0],i[1])
print("=====")
