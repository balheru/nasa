# Nasa

I decided to challenge myself a bit a use some jq, bash, awk, it's been a while

- Python/Golang/Rust versions might also appear depending on spare time

## Setup
This is tested on OSX

Since the mac has bsd date , and i generally use gnu date installing gdate will be required

brew install coreutils will give us the required gdate package

you can apply the make setup to fix this glaring issue with OSX
> make setup

### Setup Python

the virtual env should make opening the python folder in pycharm work nicely

Do not forget to set the py interpreter to 3.6+

Make commands would be next

### Make

We can use the makefile to run the scripts

For static answers to q1 and q2 and q3 we can run 
> make q1

> make q2

> make q3

If we want to run the script in a parametric style 
> make q sd=2001-01-01

> make q sd=2001-01-01 ed=2001-01-05

> make q_recent date=2001-01-01






