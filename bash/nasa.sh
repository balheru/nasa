#!/bin/sh

key="vXbhBYr4GRbrrBUQKRUtG0Jd51b298eh5tLcfyeH"
#START_DATE="2019-10-31"
#END_DATE="2019-11-02"
START_DATE="$1"
END_DATE="$2"
api_date="https://api.nasa.gov/neo/rest/v1/feed?start_date=${START_DATE}&end_date=${END_DATE}&api_key=${key}"


echo "Displa data from $1 to $2, id, name, and approach date"
echo "======================================================"
curl -s ${api_date} |\
        jq -r  '
        .near_earth_objects[][] | .id + " " + 
        .name + " " + 
        .close_approach_data[0]["close_approach_date"] + " " + 
        .close_approach_data[0]["relative_velocity"]["kilometers_per_second"]'

#We gather here the total harvested to calculate the mean later
#Since we have an api key, we are allowed to do 1000 requests per hour, we don't need to prematurely optimize
# we pass wc to xargs to strip whitespace, there is a newline somewhere in there that i don't want to debug
TOTAL_OBJECTS="$(curl -s ${api_date} | jq -r  '.near_earth_objects[][] | "Id:[" + .id + "]\tName:[ " + .name + "]\t\tClose approach date["  + .close_approach_data[0]["close_approach_date"] + "]"' | wc -l | xargs )"

# debug to check if we have the
#echo "======================================================"
#echo "lines from jq"
#echo "======================================================"
#echo "======================================================"
#echo "number of objects counted from api"
#curl -s ${api_date} | jq -r  '.element_count'



# For the velocity we sort the resulting fields using sort --numerical on the -k 4th field with the -t to delimit by "|", we chop off the last value with tail
echo "======================================================"
echo "velocity of fastest asteroid"
echo "======================================================"
curl -s ${api_date} | \
        jq -r  '.near_earth_objects[][] | .id + "|" +
        .name + "|" + 
        .close_approach_data[0]["close_approach_date"] + "|" + 
        .close_approach_data[0]["relative_velocity"]["kilometers_per_second"]' | sort -n -k4 -t'|' | tail -1


# Same situation, we just add --reverse flag to sort to get the slowest
echo "======================================================"
echo "velocity of slowest asteroid"
echo "======================================================"
curl -s ${api_date} | \
        jq -r  '.near_earth_objects[][] | .id + "|" +
        .name + "|" + 
        .close_approach_data[0]["close_approach_date"] + "|" + 
        .close_approach_data[0]["relative_velocity"]["kilometers_per_second"]' | sort -n -r -k4 -t'|' | tail -1

# We abuse awk here, passing it TOTAL_OBjECTS to calculate the sum
echo "======================================================"
echo "mean velocity of asteroids"
echo "======================================================"
curl -s ${api_date} | \
        jq -r  '.near_earth_objects[][] | 
        .id + "|" +
        .name + "|" + 
        .close_approach_data[0]["close_approach_date"] + "|" + 
        .close_approach_data[0]["relative_velocity"]["kilometers_per_second"]' \
        | awk -F "|" -v total=$TOTAL_OBJECTS '{sum+=$4;}END{print sum/total;}'


#This bit uses some inline awk that can potentially look like line noise
#I was jealous of perl
echo "======================================================"
echo "median velocity of asteroids"
echo "======================================================"
curl -s ${api_date} | \
        jq -r  '.near_earth_objects[][] | 
        .id + "|" +
        .name + "|" + 
        .close_approach_data[0]["close_approach_date"] + "|" + 
        .close_approach_data[0]["relative_velocity"]["kilometers_per_second"]' \
        | awk -F "|" '{count[NR]=$4;}END{if (NR % 2){print count[(NR + 1)/2];} else {print (count[(NR /2)] + count[(NR/2)+1]) / 2.0;}}'
# awk is sensitive to linebreaks, so i expanded it here for more clarity
#        {count[NR]=$4;}
#         END
#        {if (NR % 2){
#                print count[(NR + 1)/2];
#        } else {
#                print (count[(NR /2)] + count[(NR/2)+1]) / 2.0;
#        }}'
echo "======================================================"

# debug to easily paste the curl to the terminal
#echo ${api_date}
