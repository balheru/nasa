#!/bin/sh

key="vXbhBYr4GRbrrBUQKRUtG0Jd51b298eh5tLcfyeH"

#in this excercise we will have to call the api 1 day at a time until we get the target 3 recent events
#so we abuse the api a little instead of getting big chunks of data, and we go 1 day at a time


#2020-03-04
#yyyy-mm-dd
# i'm going to assume that objects are identified in order by their epoch date, which is very granular, since if we have more than 3 in a day we cant differentiate them by date

get_asteroids (){
curl -s ${api_date} | \
        jq -r  '.near_earth_objects[][] |
        .neo_reference_id 
        + "|" + 
        (.close_approach_data[0]["epoch_date_close_approach"]|tostring)
        + "|" + 
        .close_approach_data[0]["close_approach_date"]
        + "|" + 
        (.is_potentially_hazardous_asteroid|tostring)' \
        | sort -n -r -t"|" -k2 \
        | grep true
}

# We need to generate a valid list of dates backwards from our current date
my_array=()
#DATE="2000-01-01"
DATE="$1"
# ideally we should de a while with a break, but a for loop for 10000 days should be enough to find 3 asteroids in the data set( famous last words)
for i in {0..10000}; do 
    api_date="https://api.nasa.gov/neo/rest/v1/feed?start_date=${DATE}&end_date=${DATE}&api_key=${key}"
    if [[ ${#my_array[@]} -ge 3 ]]; then
        break
    fi
    my_array+=( $(get_asteroids) )
    DATE="$(gdate -I -d "$DATE -$i days")"
done

echo "asteroid neo id's, their epoch date, and their approach date "
echo  ${my_array[0]}
echo  ${my_array[1]}
echo  ${my_array[2]}
