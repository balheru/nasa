MAKEFLAGS += --silent
hello:
		cat README.md

setup:
	brew install coreutils
	# this install it globally, it's pretty agressive to a dev's laptop but in a cicd world, this is not bothersome
	pip3 install requests

setup_go:
	brew install go
	brew upgrade go

q:
	#We can pass variables to make, the default for end date is 7 days
	#Otherwise we need to define sd and ed
	#sd -> Start Date
	#ed -> End Date
	# AS a side note, the initial listing of dates is not sorted
	./bash/nasa.sh $$sd $$ed

q1:
#As per the question 1 we have these dates
#START_DATE="2019-10-31"
#END_DATE="2019-11-02"
	./bash/nasa.sh 2019-10-31 2019-11-02

q2:
#As per the second question we have these dates
#START_DATE="2020-09-10"
#END_DATE="2020-09-17"
	./bash/nasa.sh 2020-09-10 2020-09-17

q_recent:
#As per the third question we pick up whatever date we decide is "today"
	./bash/nasa_recent.sh $$date
	
q3:
#As per the third question we pick up whatever date we decide is "today"
	./bash/nasa_recent.sh 2001-01-01

py_q1:
	python3 ./python/main.py	

